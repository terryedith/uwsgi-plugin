#!/usr/bin/env python
# coding=utf-8

#
# Copyright (c) 2015-2018  Terry Xi
# All Rights Reserved.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from zope.sqlalchemy import register
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.schema import MetaData


metadata = MetaData()


class BaseMixin(object):

    # @classmethod
    # @declared_attr
    # def __tablename__(cls):
    #     return cls.__name__.lower()

    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8',
    }


class _BaseMixin(BaseMixin):
    pass


Base = declarative_base(metadata=metadata, cls=_BaseMixin)


def MetaInit():
    _metadata = MetaData(naming_convention=NAMING_CONVENTION)
    return _metadata


def BaseInit():
    _base = declarative_base(metadata=MetaInit(), cls=_BaseMixin)
    return _base


def get_engine(url):
    """
    生成Sqlalchemy数据引擎代理实例
    :param url: 数据库地址
    :return:
    """

    return create_engine(url)


def new_session(e):
    return register(sessionmaker(bind=e))


def create_tables(e, mod=None):
    """
    创建数据模型
    :param e: 数据引擎代理
    :param mod: 数据模块
    :return:
    """
    if mod and hasattr(mod, 'Base'):
        getattr(mod, 'Base').metadata.create_all(e)
    else:
        Base.metadata.create_all(e)
