#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015-2018  Terry Xi
# All Rights Reserved.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#


import sys
try:
    import urlparse
except:
    import urllib.parse as urlparse
from sqlalchemy import create_engine, Column, Integer, String, DateTime, Float, Boolean, Text, and_, or_, distinct, func
from sqlalchemy.pool import NullPool
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.schema import MetaData
from sqlalchemy.ext.automap import automap_base


if sys.version_info > (3, 0):
    unicode = str


metadata = MetaData()


class BaseMixin(object):

    # @classmethod
    # @declared_attr
    # def __tablename__(cls):
    #     return cls.__name__.lower()

    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8',
    }


class _BaseMixin(BaseMixin):
    pass


Base = declarative_base(metadata=metadata, cls=_BaseMixin)


# class TestModel(Base):
#     __tablename__ = 'test'

#     id = Column(Integer, primary_key=True)


_ENGINE = {}
_BASE = {}


def db_init(db_path, reset=False, create=False):
    global _ENGINE, _BASE
    if reset or db_path not in _ENGINE:
        if urlparse.urlparse(db_path).scheme:
            _ENGINE[db_path] = create_engine(db_path, poolclass=NullPool, pool_recycle=60)
        else:
            _ENGINE[db_path] = create_engine('sqlite:///{}'.format(db_path))
        if create:
            Base.metadata.create_all(_ENGINE[db_path])
        _BASE[db_path] = automap_base()
        _BASE[db_path].prepare(_ENGINE[db_path], reflect=True)
    s = sessionmaker(bind=_ENGINE[db_path])()
    setattr(s, 'base', _BASE[db_path])
    setattr(s, 'engine', _ENGINE[db_path])
    return s


def db_map(sess, table_name):
    _base = getattr(sess, 'base')
    return getattr(_base.classes, table_name)


def db_close(sess_list, commit=True):
    for dd in sess_list:
        if commit:
            dd.commit()
        dd.close()


def db_create_tables(sess, base=None):
    if not base:
        Base.metadata.create_all(sess.engine)
    else:
        base.metadata.create_all(sess.engine)
