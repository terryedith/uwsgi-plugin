#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015-2018  Terry Xi
# All Rights Reserved.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import os
import sys
import json
import time
import logging
import sqlite3
try:
    import commands
except:
    import subprocess as commands
try:
    from ConfigParser import ConfigParser
except:
    from configparser import ConfigParser

if sys.version_info > (3, 0):
    unicode = str


def _put_result(**data):
    return dict([(i, '{}'.format(j))for i, j in data.items()])


class Application(object):

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.name = json.load(
            open(os.path.join(os.path.dirname(__file__), 'app.json')))['name']
        self.version = json.load(
            open(os.path.join(os.path.dirname(__file__), 'app.json')))['version']
        self.config = self.read_config(os.path.join(
            os.path.dirname(__file__), 'app.ini'))
        self.db = None
        try:
            self.db = sqlite3.connect(self.config['app_db'])
        except:
            pass

    @classmethod
    def read_config(cls, name, section='app:main'):
        _ret = {}
        c = ConfigParser()
        c.read(name)
        if c.has_section(section):
            for i in c.options(section):
                _ret[i] = c.get(section, i)
        return _ret

    def set_logger(self, logger):
        self.logger = logger


APP = None
CONFIG = {}


def init(**plugin_config):
    global APP
    APP = Application()
    CONFIG['logger.path'] = '/tmp/{{plugin_type}}_{{plugin_name}}.log'
    CONFIG['{{plugin_type}}_{{plugin_name}}.logger.path'] = CONFIG['logger.path']


def config(**global_config):
    _config = dict()
    _config.update(CONFIG)
    _config.update(global_config)
    return _config


def start(loader, **kwargs):
    _result = dict()
    for d in loader.channel_scope:
        _result[d] = kwargs[d]
    conf = config(**loader.config)
    loader.logger.add_handler(conf['logger.path'])
    APP.set_logger(loader.logger)

    _data = kwargs[loader.current_channel]

    return _result


def stop(loader, **kwargs):
    _result = dict()
    for d in loader.channel_scope:
        _result[d] = kwargs[d]
    conf = config(**loader.config)
    loader.logger.add_handler(conf['logger.path'])
    APP.set_logger(loader.logger)

    _data = kwargs[loader.current_channel]

    return _result
