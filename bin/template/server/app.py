#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015-2018  Terry Xi
# All Rights Reserved.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import os
import sys
import time
import json
import urllib
import logging
import requests
import datetime
try:
    import urlparse
except:
    import urllib.parse as urlparse
try:
    import commands
except:
    import subprocess as commands
try:
    from ConfigParser import ConfigParser
except:
    from configparser import ConfigParser
from multiprocessing import Queue, Process
from concurrent.futures import ThreadPoolExecutor
from tornado.concurrent import run_on_executor
import tornado.web
import tornado.ioloop
import sqlite3

from .server import classprobe, call_later, call_event

if sys.version_info > (3, 0):
    unicode = str


QUEUE = Queue()


def _put_message(data):
    if data:
        QUEUE.put(data)


def _put_result(**data):
    return dict([(i, '{}'.format(j))for i, j in data.items()])


class Application(object):

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.name = json.load(open(os.path.join(os.path.dirname(__file__), 'app.json')))['name']
        self.version = json.load(open(os.path.join(os.path.dirname(__file__), 'app.json')))['version']
        self.config = self.read_config(os.path.join(os.path.dirname(__file__), 'app.ini'))
        self.rom = None
        self.db = None
        try:
            self.db = sqlite3.connect(self.config['app_db'])
        except:
            pass

    @classmethod
    def read_config(cls, name, section='app:main'):
        _ret = {}
        c = ConfigParser()
        c.read(name)
        if c.has_section(section):
            for i in c.options(section):
                _ret[i] = c.get(section, i)
        return _ret

    def set_logger(self, logger):
        self.logger = logger


_APP = Application()


def system_ports():
    return tuple(set([i.split(':')[1].split(' ')[0]
                      for i in commands.getoutput("netstat -tanp| grep LISTEN").split('\n')
                      if len(i.split(':')) > 1 and i.split(':')[1].split(' ')[0]]))


def system_new_port(start_num=50000):
    _port_list = [int(i) for i in system_ports()]
    import random
    _port = random.randint(start_num, 65535)
    while True:
        if _port not in _port_list:
            return _port
        _port += 1


_CRONTAB = False


def set_cron():
    global _CRONTAB
    if _CRONTAB:
        return
    _CRONTAB = True

    @classprobe('run')
    class Schedule(tornado.web.RequestHandler):
        executor = ThreadPoolExecutor(100)
        HOST = '127.0.0.1'
        PORT = system_new_port()

        @classmethod
        @call_later(3)
        @run_on_executor
        def run(cls):
            pass

        @classmethod
        @call_event(30 * 60)
        @run_on_executor
        def do_task(cls):
            pass


APP_ROUTE = {}
APP = None
CONFIG = {}
PID = None


def init(**plugin_config):
    CONFIG['logger.path'] = '/tmp/{{plugin_type}}_{{plugin_name}}.log'
    CONFIG['{{plugin_type}}_{{plugin_name}}.port'] = system_new_port()
    CONFIG['{{plugin_type}}_{{plugin_name}}.host'] = '127.0.0.1'
    c = ConfigParser()
    c.read(os.path.join(os.path.dirname(__file__), 'app.ini'))
    CONFIG['{{plugin_type}}_{{plugin_name}}.db_path'] = c.get('app:main', 'app_db')

    conf = config(**plugin_config)
    CONFIG.update(conf)

    if not urlparse.urlparse(CONFIG['{{plugin_type}}_{{plugin_name}}.db_path']).scheme:
        CONFIG['{{plugin_type}}_{{plugin_name}}.db_path'] = os.path.join(os.path.dirname(__file__),
                                                                       CONFIG['{{plugin_type}}_{{plugin_name}}.db_path'])
    Application.db_path = CONFIG['{{plugin_type}}_{{plugin_name}}.db_path']


def config(**global_config):
    c = dict()
    c['logger.path'] = global_config.get('logger.path', CONFIG['logger.path'])
    c['{{plugin_type}}_{{plugin_name}}.logger.path'] = global_config.get('{{plugin_type}}_{{plugin_name}}.logger.path', CONFIG['logger.path'])
    c['{{plugin_type}}_{{plugin_name}}.host'] = global_config.get(
        '{{plugin_type}}_{{plugin_name}}.host',
        CONFIG['{{plugin_type}}_{{plugin_name}}.host'])
    c['{{plugin_type}}_{{plugin_name}}.port'] = global_config.get(
        '{{plugin_type}}_{{plugin_name}}.port',
        CONFIG['{{plugin_type}}_{{plugin_name}}.port'])
    return c


def start(loader, **kwargs):
    global _APP
    loader.logger.warn('start')
    _conf = config(**loader.config)
    set_cron()
    loader.logger.add_handler(_conf['logger.path'])
    loader.logger.add_handler(_conf['{{plugin_type}}_{{plugin_name}}.logger.path'])
    _APP.set_logger(loader.logger)
    # _APP.rom.update(**kwargs[loader.config_channel])

    _data = kwargs[loader.current_channel]
    loader.logger.warn('end')
    return {'result': _data, 'data': kwargs['data'],
            loader.config_channel: kwargs[loader.config_channel]}


def start_job(loader, **kwargs):
    loader.logger.warn('start')
    from .server import make_route, make_app
    _result = dict()
    c = config(**loader.config)
    set_cron()
    loader.logger.add_handler(c['logger.path'])
    loader.logger.add_handler(c['{{plugin_type}}_{{plugin_name}}.logger.path'])
    _APP.set_logger(loader.logger)
    # _APP.rom.update(**kwargs[loader.config_channel])

    def start_app():
        global APP
        route = make_route(**APP_ROUTE)
        APP = make_app(route)
        APP.listen(int(c['{{plugin_type}}_{{plugin_name}}.port']), str(c['{{plugin_type}}_{{plugin_name}}.host']))
        tornado.ioloop.IOLoop.current().start()

    global PID
    if not PID:
        p = Process(target=start_app)
        p.daemon = True
        p.start()
        PID = int(str(p.pid))

    import signal

    def kill_process(*args, **kwargs):
        try:
            os.kill(PID, signal.SIGKILL)
        except:
            pass

    signal.signal(signal.SIGINT, kill_process)

    while True:
        d = QUEUE.get()
        if d:
            _result['result'] = d
            _result['data'] = d
            _result[loader.config_channel] = kwargs[loader.config_channel]
            loader.logger.warn('end')
            yield _result
