# 数据中心部署说明

维护人 | 版本号 | 描述 | 日期
--- | --- | --- | ---
奚驰俊 | 0.1 | 初版 | 2019-05-22

## 数据中心部署节点介绍
* 数据采集缓存节点
* 数据服务数据库节点
* 数据处理节点
* 数据存储节点
* 数据总线节点

## 数据安装包介绍
* spark_server_core.zip(数据处理节点)
* spark_server_com.zip(数据处理节点)
* WizDataCenter.zip(数据处理节点)
* WizDataManager.zip(数据处理节点)
* tool_spark.zip(数据处理节点)
* presto_server.zip(数据存储节点)
* database_presto.zip(数据存储节点)
* WizDataStore.zip(数据存储节点)
* kafka_server.zip(数据总线节点)
* WizSpider.zip(数据总线节点)
* WizCollect.zip(数据总线节点)

## 数据采集缓存节点部署
### docker部署
```shell
# 通过docker命令启动mongodb容器，设置服务端口为27010
docker run -p 27010:27017 -v $PWD/db:/data/db -d mongo
# 通过docker命令启动mysql容器，设置服务端口为23300（此端口用于报表系统）
docker run -p 23300:3306 -v $PWD/conf:/etc/mysql/conf.d -v $PWD/logs:/logs -v $PWD/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=12345678 -d mysql:5.7
# 通过docker命令启动postgresql容器，设置服务端口为25430
docker run -e POSTGRES_PASSWORD=12345678 -p 25430:5432 -d postgres:9.6 
```

## 数据服务数据库节点部署
### docker部署
```shell
# 通过docker命令启动容器，设置服务端口为13306，密码为12345678
docker run -p 13306:3306 -v $PWD/conf:/etc/mysql/conf.d -v $PWD/logs:/logs -v $PWD/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=12345678 -d mysql:5.7

# 通过本地客户端登陆
mysql -u root -p12345678 -h 127.0.0.1 -P13306
```

## 数据总线节点部署

### docker部署

```shell
## 通过安装脚本启动一个名为pcloud的容器并添加端口14000，14001，14002，14003，删除端口30001到30005
./install_sys.sh install_docker --docker-category master --work-dir /tmp/install --install-zip kafka_server.zip  --install-log /tmp/install.log --docker-name pcloud --docker-ports '+14000:14000,+14001:14001,+14002:14002,+14003:14003,-30001:30001,-30002:30002,-30003:30003,-30004:30004,-30005:30005'
```

### 服务部署

```shell
## WizCollect服务部署
## 容器进入服务目录
cd /opt/app
## 将WizCollect.zip解压并进入目录
cd WizCollect
## 配置数据库地址参数sql.url(mysql://root:12345678@172.18.0.1:13306/collect?charset=utf8)
## 运行服务组件安装脚本
./install.sh
## 生成配置文件(以停车场采集为例)
cd sbin
./build_config -o plugin.ini -s 172.18.0.4 -c test_channel -r rom_wy_pms -p collect_pms
## 运行服务
./run_ini.sh development.ini 14001

## WizSpider服务部署
## 容器进入服务目录
cd /opt/app
## 将WizSpider.zip解压并进入目录
cd WizSpider
## 配置数据库地址参数sql.url(mysql://root:12345678@172.18.0.1:13306/spider?charset=utf8)
## 运行服务组件安装脚本
./install.sh
## 生成配置文件(以停车场采集为例)
cd sbin
./build_config -o plugin.ini -s 172.18.0.4 -c test_channel -r rom_wy_pms --mongodb-host 172.18.0.1 --mongodb-port 27010 --mongodb-db wiseonline
## 运行服务
./run_ini.sh development.ini 14000
```



## 数据处理节点部署

### docker部署
```shell
## 通过安装脚本启动一个名为ccloud的容器并添加端口10001，10010，10011
./install_sys.sh install_docker --work-dir /tmp/install --install-zip spark_server.zip --server-zip spark_server.zip,spark_server_com.zip --install-log /tmp/install.log --copy-files WizDataManager.zip,WizDataCenter.zip --docker-name ccloud --docker-ports '+10010:10010,+10011:10011,+10001:10001'
```
### 服务部署
```shell
## WizDataManager服务部署
## 容器进入服务目录
cd /opt/app
## 将WizDataManager.zip解压并进入目录
cd WizDataManager
## 配置数据库地址参数sql.url(mysql://root:12345678@172.18.0.1:13306/datamanager?charset=utf8)
## 运行服务组件安装脚本
./install.sh
soure env/bin/active
python setup.py develop
python setup.py install
## 运行服务
./run_ini.sh development.ini 10001

## WizDataCenter服务部署
## 将WizDataCenter.zip解压并进入目录
cd WizDataCenter
## 配置数据库地址参数sql.url(mysql://rconfoot:12345678@172.18.0.1:13306/datacenter?charset=utf8)
## 运行服务组件安装脚本conf
./install.shconf
soure env/bin/active
python setup.py develop
python setup.py install
apt-get install redis-server
## gitlab进入uwsgi_plugin库打包插件
./publish_platform test_plugin.json
## 将spark插件放入服务
cp tool_spark.zip DataCenter/plugins
## 运行服务
./run_ini.sh development.ini 10011
./sbin/run_redis_daemon
./bin/start_work  development.ini
```

## 数据存储节点部署
### docker部署
```shell
## 通过安装脚本启动一个名为dcloud的容器并添加端口12000，12001，删除端口30001到30005
./install_sys.sh install_docker --docker-category all --work-dir /tmp/install --install-zip presto_server.zip  --install-log /tmp/install.log --copy-files WizDataStore.zip --docker-name dcloud --docker-ports '+12000:12000,+12001:12001,-30001:30001,-30002:30002,-30003:30003,-30004:30004,-30005:30005'
```
### 服务部署
```shell
## WizDataStore服务部署
## 进入容器内目录
cd /opt/system/presto_system
## 连接本地hive
./connect_hive.sh catalog 127.0.0.1 9083
## 连接mongodb缓存
./connect_mongodb.sh catalog 172.18.0.1 27010
## 连接mysql缓存
./connect_mysql.sh catalog 172.18.0.1 23300 root 12345678
## 连接postgresql缓存(wuye库提前手动建好)
./connect_postgresql.sh catalog 172.18.0.1 25430 postgres 12345678 wuye
## 回到宿主机导入postgresql数据（可不做）
psql -dwuye -h127.0.0.1 -p25430 -Upostgres -fwuye-test20190313.backup
## gitlab进入uwsgi_plugins库tools分支打包生成客户端脚本
cd templates/tools
./publish_script
## 使用客户端脚本录入设备（如停车场设备,新类型需要小量开发）
./cli_datastore.sh prekit_init --mysql-address 172.18.0.1 --mysql-port 13306 --mysql-user root --mysql-password 12345678 --mysql-database prekit --presto-address 127.0.0.1 --presto-port 20000
./cli_dataregister.sh register_device --source-ip 127.0.0.1 --source-port 8888 --define-system-name jiaxin_wuy --device-select PMS --define-type-name wuye_pms
./cli_dataregister.sh register_device --source-ip 127.0.0.1 --source-port 8888 --define-system-name jiaxin_wuy --device-select PMS_GATE --define-type-name wuye_pms_gate
## 使用客户端脚本生成插件配置插件（以下操作用于采集服务,最后生成配置插件(rom_wy_pms.zip)）
./cli_datastore2device.sh run --type-id e2b15c61-086a-4869-957e-a4c4a562001a --file-name wy_pms.xls
./cli_datadevice.sh parse --target-host 127.0.0.1 --target-port 10001 --config cli_datadevice.ini --plugin collect_wizPMS --target-file wy_pms.xls
./cli_datadeivce.sh download --target-host 127.0.0.1 --target-port 10001--project-id 28972500-4652-4c02-86e3-815a8a90e49f --output-name wy_pms
## 使用客户端脚本查看设备信息
virtualenv env --no-site-packages
source env/bin/active
apt-get install libmysqlclient-dev
pip install -r cli_datastore.txt
./cli_datastore.sh prekit_system_list
./cli_datastore.sh prekit_target_type_list
./cli_datastore.sh prekit_target_list
## 回到容器进入服务目录
cd /opt/app
## 将WizDataStore.zip解压并进入目录
cd WizDataStore
## 配置数据库地址参数sql.url(mysql://root:12345678@172.18.0.1:13306/datastore?charset=utf8)
## 配置数据库地址参数target.url(mysql://root:12345678@172.18.0.1:13306/prekit?charset=utf8)
## 运行服务组件安装脚本
./install.sh
soure env/bin/active
python setup.py develop
python setup.py install
apt-get install redis-server
## gitlab进入uwsgi_plugin库master分支打包插件
./publish_platform test_plugin.json
## 将presto插件放入服务
cp database_presto.zip DataStore/plugins
## 运行服务
./run_ini.sh development.ini 12001
./sbin/run_redis_daemon
./bin/start_work  development.ini
```
