# Jenkins部署流程

维护人 | 版本号 | 描述 | 日期
--- | --- | --- | ---
奚驰俊 | 0.1 | 初版 | 2019-06-28

##下载容器包

* 下载http://222.73.202.77/python_system/kafka_server.git下zip包并重命名为kafka_server.zip

* 下载http://222.73.202.77/python_system/presto_server_core.git下zip包并重命名为presto_server.zip

* 下载http://222.73.202.77/python_system/spark_server.git下zip包并重命名为spark_server.zip

* 下载http://222.73.202.77/python_system/spark_server_com.git下zip包并重命名为spark_server_com.zip

将以上所有包放在/tmp/zip目录（该目录可自定）

## 下载插件中心和配置文件

```shell
git clone http://222.73.202.77/chijun/uwsgi_plugins.git
git clone http://222.73.202.77/chijun/uwsgi_config.git
```

## 生成服务包

```shell
cd uwsgi_plugins
./publish_platform uwsgi_config/basic/test_collect.json
./publish_platform uwsgi_config/basic/test_spider.json
./publish_platform uwsgi_config/basic/test_datacenter.json
./publish_platform uwsgi_config/basic/test_datamanager.json
./publish_platform uwsgi_config/basic/test_datastore.json
./publish_platform uwsgi_config/basic/test_control.json
./publish_platform uwsgi_config/basic/test_register.json
cp ./target/WizDataCenter.zip /tmp/zip
cp ./target/WizDataManager.zip /tmp/zip
cp ./target/WizDataStore.zip /tmp/zip
cp ./target/WizDataControl.zip /tmp/zip
cp ./target/WizDataRegister.zip /tmp/zip
cp ./target/WizCollect.zip /tmp/zip
cp ./target/WizSpider.zip /tmp/zip
```

## 安装容器

```shell
cd uwsgi_plugins
git checkout tools
./publish_script
./upload_script run --config uwsgi_config/basic/upload_script.json
# 安装基础数据库容器
./install_cloud deploy_database_node
# 安装外部数据库容器（用于提供psql服务）
./install_cloud deploy_cache_node
# 安装总线容器及服务
./install_cloud deploy_bus_node --out-zip-dir /tmp/zip
# 安装数据中心容器及服务
./install_cloud deploy_center_node --out-zip-dir /tmp/zip
# 安装数据处理容器及服务
./install_cloud deploy_process_node --out-zip-dir /tmp/zip
# 安装数据存储容器及服务
./install_cloud deploy_storage_node --out-zip-dir /tmp/zip
```

